<?php
// $Id$

/**
 * @file
 * @author e-Commerce Dev Team
 * Create file products. Enable the download only after the
 * payment and set date to expire.
 */

/**
 * Add a new field on each Attachment. This will mark the file
 * as a product
 *
 * @param &$form
 *   Array, the form array
 * @param $files
 *   Array, the form fields that deals with file attachments
 */
function _ec_file_form_change(&$form, $files, $form_id) {
  foreach (element_children($files) as $id) {
    $result = db_result(db_query('SELECT fid FROM {ec_file} WHERE nid = %d and fid = %d', $form['nid']['#value'], $id));
    $files[$id]['product'] = array(
      '#default_value'  => $result,
      '#type'           => 'checkbox'
      );
  }

  // Overwrite the theme function upload_form_current
  $files['#theme'] = 'ec_file_upload_form';

  if ($form_id == 'upload_js') {
    $form['files'] = $files;
  }
  else {
    $form['attachments']['wrapper']['files'] = $files;
  }
}

/**
 * Sends a logged in user to their purchased file listing.
 */
function ec_file_redirect_to_display_files() {
  global $user;
  drupal_goto("user/$user->uid/store/myfiles");
}

/**
 * Display a user's purchased files as a table list.
 *
 */
//TODO: do we need $user?
function ec_file_display_purchased_files_as_table($user) {
  $files = ec_file_purchased_files($user);
  return theme('ec_file_myfiles', $files);
}


/**
 * Display a user's purchased files in a block.
 *
 */
//TODO: do we need $user?
function ec_file_display_purchased_files_as_block($user) {
  $files = ec_file_purchased_files($user);
  return theme('ec_file_myfiles_link_list', $files);
}
