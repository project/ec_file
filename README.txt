$Id

********************************************************************
DESCRIPTION:

Creates file download products for e-Commerce.


********************************************************************
USAGE:

After paying for a file/s, the customer will access them by clicking 
on the MY ACCOUNT > STORE > MY FILES link after they login.
You should probably add these instructions to the e-mail users 
receive after making a purchase. The default instructions are here:

ADMINISTER > e-Commerce configuration > Store > Workflow > completed > edit

Customers are be able to download files after the payment status
has been marked 'completed' for their transaction. Typically this
happens immediately after payment, but in cases of e-checks and
other payment types, this could be a couple of days.

If you are using cron on your site, transactions that consist
only of non-shippable items - such as file downloads - will have
their workflow be moved from 'pending' to 'completed'. This saves
you the time of doing this manually.


********************************************************************
SETUP:

1. Install the e-Commerce module from
   http://drupal.org/project/ecommerce

2. Download the private upload module from 
   http://drupal.org/project/private_upload
   enable it and take a look at its settings:

   ADMINISTER > SITE CONFIGURATION > PRIVATE UPLOADS

3. Enable the ec_file module.

4. If you haven't already, create a Content type for your file download by going to
   ADMINISTER > CONTENT MANAGEMENT > CONTENT TYPES > ADD CONTENT TYPE

5. Now add a Product type to link to your Content type by going to:
   ADMINISTER > E-COMMERCE CONFIGURATION > PRODUCTS
   and click on "Add".

6. Give your file download product type a name, like "File download."
   In "Type" put in something like "file_download".
   Put in a description and select the content type/s which you'd like to enable file downloads for and click "Submit product type".

7. Edit your new product type and click "Add feature". Select
   "File download product" and click "Add product feature."

8. Edit your file download content type and under "Product" select the product type you've just created and click "Save content type".


Great! You now have a content type and a product type which lets a customer buy your file download product.
Take a moment to bask in the glow of your monitor.


********************************************************************
CREATING A NEW FILE DOWNLOAD PRODUCT TO SELL:

To create a node with a file attachment you can sell, go to
CREATE CONTENT > YOUR-CONTENT-TYPE
and under "File attachments" upload a file. Make sure that at least "Private" and "Sell" are checked. Save your node.
Customers can now buy your file download by clicking "Add to cart" on your node.

You can change the permissions for which roles can create a file download at 
ADMINISTER > USER MANAGEMENT > PERMISSIONS


********************************************************************
SETTINGS:

The settings page is at
ADMINISTER > E-COMMERCE CONFIGURATION > FILE DOWNLOADS

File uploads can be made sellable by default, meaning that as soon as they are uploaded the "Sell" checkbox
is automatically checked.

The "List", "Private" and "Sell" checkboxes are hidden by default. You can show them by changing the
'show upload info checkboxes' permission in ADMINISTER > USER MANAGEMENT > PERMISSIONS
This is useful if you want to allow your users to upload sellable files to your site and don't want to let
them change the options associated with that file. You'll usually need to create a new user role for this.


********************************************************************
See README.txt in e-Commerce module root for other info.


********************************************************************
CREDITS:

brmassa coded ec_file for ecommerce 3.x. 
Roger Saner updated it to 4.x with guidance from gordon.
darrenoh contributed some code from his custom song_file.