<?php
// $Id$

/**
 * @author e-Commerce Dev Team
 * @file
 * Implement required theme functions for ec_file
 */

/**
 * Theme the attachments list. Based on theme_upload_form_current.
 *
 * Recreate the upload fieldset on node's creation and on file uploads
 * to include the product checkbox. Only if the user permission of
 * 'show upload info checkboxes' is set then List, Private and Sell will show.
 *
 * If the default file upload setting at admin/ecsettings/ec_file is set to 'Yes'
 * then every file upload is automatically made a product.
 *
 * @ingroup theme
 */
function theme_ec_file_upload_form($form) {
  if (user_access('show upload info checkboxes')) {
    $header = array('', t('Delete'), t('List'), t('Private'), t('Sell'), t('Description'), t('Weight'), t('Size'));
  }
  else {
    $header = array('', t('Delete'), t('List'), t('Description'), t('Weight'), t('Size'));
  }

  drupal_add_tabledrag('upload-attachments', 'order', 'sibling', 'upload-weight');

  foreach (element_children($form) as $key) {
    // Add class to group weight fields for drag and drop.
    $form[$key]['weight']['#attributes']['class'] = 'upload-weight';

    $row = array('');
    $row[] = drupal_render($form[$key]['remove']);
    $row[] = drupal_render($form[$key]['list']);
    if (user_access('show upload info checkboxes')) {
      $row[] = drupal_render($form[$key]['private']);
      $row[] = drupal_render($form[$key]['product']);
    }
    else {
      $form[$key]['list']['#type'] = 'hidden';
      $form[$key]['private']['#type'] = 'hidden';
      $form[$key]['product']['#type'] = 'hidden';
    }
    $row[] = drupal_render($form[$key]['description']);
    $row[] = drupal_render($form[$key]['weight']);
    $row[] = drupal_render($form[$key]['size']);
    $rows[] = array('data' => $row, 'class' => 'draggable');
  }
  $output = theme('table', $header, $rows, array('id' => 'upload-attachments'));
  $output .= drupal_render($form);
  return $output;
}

/**
 * List all files purchased and for download in a table list
 *
 * @param $files_list
 *   Array, the list of files
 * @return
 *   String, the HMTL with all files purchased
 * @ingroup theme
 */
function theme_ec_file_myfiles($files_list) {
  $header = array(
    array('data' => t('Filename'), 'field' => 't.txnid', 'sort' => 'desc'),
    array('data' => t('Size'), 'field' => 'epf.size'),
    //array('data' => ($expired) ? t('Expired') : t('Expires'), 'field' => 'ere.expiry'),
    array('data' => t('Transaction'), 'field' => 'etp.txnid')
  );

  foreach ($files_list as $expired => $files) {
    $rows = array();
    foreach ($files as $file) {
      $rows[] = ec_file_format_file($file, $private_upload_path);
    }

    if (empty($files)) {
      if (empty($expired)) {
        $output .= t('You have no files to download.');
      }
    }
    else {
      /* if (!empty($expired)) {
        $output .= '<h3>'. t('Expired files') .'</h3>';
      }*/
      $output .= theme('table', $header, $rows);
    }
  }
  return $output;
}

/**
 * List all files purchased and for download in an html list of links
 *
 * @param $files_list
 *   Array, the list of files
 * @return
 *   String, the HMTL with all files purchased
 * @ingroup theme
 */
function theme_ec_file_myfiles_link_list($files_list) {
  foreach ($files_list as $expired => $files) {
    $rows = array();
    foreach ($files as $file) {
      //if (!empty($file->expiry)) {
        $rows[] = ec_file_format_file($file, $private_upload_path);
      //}
    }

    //TODO: implement file expiry on downloads
    if (empty($files)) {
      if (empty($expired)) {
        $output .= t('You have no files to download.');
      }
    }
    else {
      if (!empty($expired)) {
        $output .= '<h3>'. t('Expired files') .'</h3>';
      }
      foreach ($rows as $row) {
        $output .= $row[0] . '<br/>';
      }
    }
  }
  return $output;
}

/**
 * Formats a file array nicely.
 *
 * @return
 *   Array, of the following elements:
 *     File title, file size, expiry date, link to download file
 */
function ec_file_format_file($file) {
  $workflow = ec_store_transaction_workflow('type', $file->workflow); //type_code?
  //TODO: test for when the upload path is something more complicated that 'private', like 'private/secret' for eg.
  $private_upload_path = variable_get('private_upload_path', 'private');
  $pos = strpos($file->filepath, $private_upload_path) + 8;
  $file->file = substr($file->filepath, $pos);
  // Set the file name
  //$file->title .= ' ('. preg_replace('|^[^/]*/|', '', $file->filepath) .')'; //returns something like default/files/something/private/LICENSE.txt
  $file->title .= ' ('. $file->file .')'; //returns something like LICENSE.txt
  if ($workflow['type'] == EC_WORKFLOW_TYPE_COMPLETE and empty($expired)) {
    $file->title = l($file->title, 'system/files/'. $private_upload_path .'/'. $file->file);
  }
  else {
    $file->title = theme('placeholder', $file->title);
  }

  // Create the expiration data string
/*
  if (empty($file->expiry)) {
    $file->expiry = 'Never';
  }
  else {
    if (!empty($expired)) {
      $file->expiry = t('%interval ago',
        array('%interval' => format_interval($file->expiry - time(), 3)));
    }
    else {
      $file->expiry = format_interval($file->expiry - time(), 3);
    }
  }
  */

  return array($file->title, format_size($file->filesize), l($file->txnid, 'store/transaction/'. $file->txnid .'/view'));
}